import math

import requests
from bs4 import BeautifulSoup
import os
from xlutils.copy import copy as xlcopy
import xlrd
import datetime
import httplib2
import sys
from fake_useragent import UserAgent
from time import sleep

from resources.resources_in import return_data
import get_and_check_start_ids as gtc


links = []


# получить html код страницы
def get_html(url):
    """ :return исходный HTML код страницы """
    r = requests.get(url, headers={'User-Agent': UserAgent().chrome})

    return r.text


# просто для теста
# microUSBвв
# https://opt-list.ru/catalog/USB-Micro-USB?&page=1

# получить кол-во страниц
def get_total_pages(html):
    """ :return кол-во страниц для дальнейшего парсинга """
    soup = BeautifulSoup(html, 'html.parser')

    try:
        pages = soup.find('div', class_='pages').find_all('a')[-1].get('href')
        total_pages = pages.split('=')[-1]
        return int(total_pages)
    except:
        return 1




def get_links(url):
    """ сбор ссылок для последующего парсинга товаров """
    global links

    html = get_html(url)
    soup = BeautifulSoup(html, 'html.parser')
    divs = soup.find_all('div', class_='item-inner')

    for div in divs:
        href = div.find('a')['href']

        links.append(href)



# Парсинг самих полей товара
def parse_product():
    """ парсинг товаров и занесение итогов в XLS файл """
    now = datetime.datetime.now()
    full_date = now.strftime("%Y-%m-%d %H:%M:%S")
    date = now.strftime("%Y-%m-%d")

    # чтение и запись в файл
    path_file_xls = os.path.abspath("resources\parse.xls")
    path_file_out_xls = os.path.abspath("OUT\parse " + date + ".xls")

    start_id = gtc.get_start_id()
    product_id = gtc.get_start_id()
    line = 1  # строка, в котору будет идти запись

    book = xlrd.open_workbook(path_file_xls, on_demand=True, formatting_info=True)

    write_book = xlcopy(book)
    write_sheet = write_book.get_sheet(0)

    gtc.check_id_error()

    global links

    input_data = return_data()

    for date_in in range(0, len(input_data)):
        base_url = input_data[date_in][0]
        category = input_data[date_in][1]
        image_directory = input_data[date_in][2]
        page_path = r'?&page='

        # А дальше 3.14...

        while True:
            try:
                total_pages = get_total_pages(get_html(base_url + "?&page=1"))

                for i in range(1, total_pages + 1):
                    url_gen = base_url + page_path + str(i)
                    # get all links
                    get_links(url_gen)

                    print(url_gen)

                # ТУТ

                for link in links:

                    print(link)

                    while True:
                        try:
                            html = get_html(link)
                            soup = BeautifulSoup(html, 'html.parser')

                            # name
                            # # self.categories = categories
                            # image
                            # image_url
                            # image_directory
                            # price
                            # description
                            # stock

                            # pages = soup.find('div', class_='pagination').find_all('a')[-1].get('href')

                            availability_check = str(soup.find('div', class_='available-true'))
                            if 'display:none' in availability_check:
                                break
                            else:
                                name = soup.find('h1', attrs={'itemprop': 'name'}).text
                                try:
                                    image_url = soup.find('div', class_='general-img').find('img').get('src')
                                    image_name = \
                                        soup.find('div', class_='general-img').find('img').get('src').split('/')[-1].split(
                                            '?')[0]
                                    print(image_name)
                                    # save product photo to "/photo/"
                                    response = requests.get(image_url, headers={'User-Agent': UserAgent().chrome})
                                    path_to_programm_folder = os.getcwd()
                                    if response.status_code == 200:
                                        try:
                                            h = httplib2.Http('.cache')
                                            response, content = h.request(image_url)
                                            out = open((path_to_programm_folder + '\photo' + image_directory + image_name),
                                                       'wb')
                                            out.write(content)
                                            out.close()
                                        except FileNotFoundError:
                                            path_to_programm_folder = os.getcwd()
                                            folder_name = path_to_programm_folder + '\photo' + image_directory
                                            os.mkdir(folder_name)
                                            print('Папка создана!')
                                            h = httplib2.Http('.cache')
                                            response, content = h.request(image_url)
                                            out = open((path_to_programm_folder + '\photo' + image_directory + image_name),
                                                       'wb')
                                            out.write(content)
                                            out.close()
                                except:
                                    image_url = ''
                                    image_name = ''
                                    print('[Фото отсутсвует]')

                                # -------------------------------------------------------
                                # h = httplib2.Http('.cache')
                                # response, content = h.request(image_url)
                                
                                # out = open( (path_to_program_folder + '\photo' + image_directory + image_name), 'wb' )
                                # out.write(content)
                                # out.close()

                                # вытянул строку и отсёк лишние 2 ноля через split
                                # price = soup.find('div', class_='price').find('spa
                                # n', class_='num').text.strip().split(',')[0]
                                price = soup.find('span', attrs={'itemprop':
                                                                     'price'}).text.split(',')[0].replace(' ', '')
                                price = int(price)

                                try:
                                    description = soup.find('div', class_='htmlDataBlock').text.strip()
                                except:
                                    description = name
                                if '&nbsp;' in description:
                                    description = description.replace('&nbsp;', '')

                                # TODO доработать. Тут ****, а не код
                                # проверка ниличия товара
                                stock = False
                                availability_check = str(soup.find('div', class_='available-true'))
                                if 'display:none' in availability_check:
                                    stock = False
                                else:
                                    stock = True
                                    # layout в таблицу
                                    write_sheet.write(line, 34, '0:')

                                # # TODO for test
                                # print(name)
                                # print(image_url)
                                # print(image_name)
                                # print(price)
                                # print(description)

                                # print('\n')
                                # print('id = ' + str(product_id))
                                # print(link)
                                # print(name)
                                # print(image_url)
                                # print(image_name)
                                # print(price)
                                # print(description)
                                # print(stock)

                                # id в таблицу
                                write_sheet.write(line, 0, product_id)
                                # name в таблицу
                                write_sheet.write(line, 1, name)
                                write_sheet.write(line, 11, name)
                                write_sheet.write(line, 29, name)
                                write_sheet.write(line, 30, name)
                                # category в таблицу

                                # TODO ТУТ Я ВНЕС ИЗМЕНЕНИЯ

                                write_sheet.write(line, 2, (category))
                                # quantity в таблицу
                                write_sheet.write(line, 10, 999)
                                # TODO поправть. ТОЛЬКО ДЛЯ ТЕСТА
                                # image_name в таблицу
                                write_sheet.write(line, 13, ('/catalog' + image_directory + image_name))
                                # shipping в таблицу
                                write_sheet.write(line, 14, "yes")
                                # price в таблицу
                                price = math.ceil(price * 1.03)
                                write_sheet.write(line, 15, price)
                                # points в таблицу
                                write_sheet.write(line, 16, 0)
                                # date (загрузки и модификации) в таблицу
                                write_sheet.write(line, 17, full_date)
                                write_sheet.write(line, 18, full_date)
                                # date_available в таблицу. По умолчанию актуальность товара стоит 30 дней
                                # TODO посмотреть, что за дичь
                                write_sheet.write(line, 18, full_date)
                                # full_out_date = datetime.datetime.now() + datetime.timedelta(days=30)
                                # full_out_date_str = full_out_date.strftime("%Y-%m-%d %H:%M:%S")
                                # write_sheet.write(line, 19, full_out_date_str)
                                # weight в таблицу
                                write_sheet.write(line, 20, 0)
                                # weight_unit в таблицу
                                write_sheet.write(line, 21, "кг")
                                # length в таблицу
                                write_sheet.write(line, 22, 0)

                                print(product_id)

                                # width в таблицу
                                write_sheet.write(line, 23, 0)
                                # height в таблицу
                                write_sheet.write(line, 24, 0)
                                # length_unit в таблицу
                                write_sheet.write(line, 25, "см")
                                # status в таблицу
                                write_sheet.write(line, 26, "true")
                                # tax_class_id в таблицу
                                write_sheet.write(line, 27, 0)
                                # description(ru-ru) в таблицу
                                write_sheet.write(line, 28, description)
                                # stock_status_id в таблицу
                                # TODO заменил проверку stock. Она теперь выше
                                # здесь код должен отработаться только на тот товара, который в наличии
                                # у оптлиста $#!@#$!!!!
                                write_sheet.write(line, 32, 7)
                                # store_ids в таблицу
                                write_sheet.write(line, 33, 0)

                                # sort_order в таблицу
                                write_sheet.write(line, 37, 1)
                                # subtract в таблицу
                                write_sheet.write(line, 38, "true")
                                # minimum в таблицу
                                write_sheet.write(line, 39, 1)

                                print(datetime.datetime.now())

                                product_id += 1
                                line += 1
                                break
                        except:
                            print("Оптлист сволочи...")
                            sleep(20)

                write_book.save(path_file_out_xls)
                print("ГОТОВО")

                # Очистка links[]
                links.clear()
                break
            except:
                print('Оптлист совсем очещуели!')
                sleep(20)

    #  функция для записи в последнюю страницу (ProductSEOKeywords)
    # ААААААААААРРРРРРРРРР!!!!!!!!!
    try:
        path_file_not_ready = os.path.abspath("OUT\parse " + date + ".xls")
        path_to_save_ready_file = os.path.abspath("OUT\parseREADY " + date + ".xls")

        book = xlrd.open_workbook(path_file_not_ready, on_demand=True, formatting_info=True)
        print("Файл открыт успешно")

        write_book_for_last_page = xlcopy(book)
        write_sheet_for_last_page = write_book_for_last_page.get_sheet(9)
        print("Write sheet open")

        # сама запись product_id в ProductSEOKeywords
        num_of_line = 1
        # count = product_id - start_id

        # for z in range (0, count):
        #     write_sheet_for_last_page.write(num_of_line, 0, start_id)
        #     start_id += 1
        #     num_of_line += 1

        for i in range(start_id, (product_id)):
            write_sheet_for_last_page.write(num_of_line, 0, i)
            # store_id  таблицу
            write_sheet_for_last_page.write(num_of_line, 1, 0)
            num_of_line += 1
            print(i)

        write_book_for_last_page.save(path_to_save_ready_file)

        # save id for next parse
        gtc.save_id(str(product_id + 1))

    except FileNotFoundError:
        print("что за ?!?!??!?!")
        sys.exit(-404)


if __name__ == '__main__':
    parse_product()
