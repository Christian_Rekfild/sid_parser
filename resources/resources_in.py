input_data = (
    # (
    #     (r'https://opt-list.ru/catalog/USB-Micro-USB'), #base_url
    #     ("144"), # category
    #     ('/microUSB/') # image_directory
    # ),
    (
        #DVD-R(RW)/CD-R(RW)
        (r'https://opt-list.ru/catalog/DVD-R-CD-R'),
        ("61"),
        ('/DVD-R/')
    ),
    (
        #USB РЕПЛИКА (НИЗКАЯ СКОРОСТЬ ЧТЕНИЯ, ТОЛЬКО ДЛЯ ТЕКСТА)
        (r"https://opt-list.ru/catalog/USB-RePLIKA-NIZKAya-SKOROST-ChTeNIya-TOLKO-DLya-TeKSTA"),
        ("63"),
        ('/usb_flash_low_dpeed_only_for_text/')
    ),
    (
        # USB 4 GB
        (r"https://opt-list.ru/catalog/4-GB"),
        ("64"),
        ('/usb_4gb/')
    ),
    (
        # USB 8 GB
        (r"https://opt-list.ru/catalog/8-GB"),
        ("65"),
        ('/usb_8gb/')
    ),
    (
        # USB 16 GB
        (r"https://opt-list.ru/catalog/16-GB"),
        ("66"),
        ('/usb_16gb/')
    ),
    (
        # USB 32 GB
        (r"https://opt-list.ru/catalog/32-GB"),
        ("67"),
        ('/usb_32gb/')
    ),
    (
        # USB 64 GB
        (r"https://opt-list.ru/catalog/64-GB"),
        ("68"),
        ('/usb_64gb/')
    ),
    (
        # USB 128 / 256 GB
        (r"https://opt-list.ru/catalog/USB-128-GB"),
        ("69"),
        ('/usb_128_256gb/')
    ),
    (
        # USB Тематические флешки (подарочные)
        (r"https://opt-list.ru/catalog/Tematicheskie-fleshki"),
        ("70"),
        ('/prize_usb/')
    ),
    (
        # MICROSD РЕПЛИКА (НИЗКАЯ СКОРОСТЬ ЧТЕНИЯ, ТОЛЬКО ДЛЯ ТЕКСТА)
        (r"https://opt-list.ru/catalog/MICROSD-RePLIKA-NIZKAya-SKOROST-ChTeNIya-TOLKO-DLya-TeKSTA"),
        ("72"),
        ('/microSD_low_speed/')
    ),
    (
        # MicroSD 2 GB
        (r"https://opt-list.ru/catalog/MicroSD-2-GB"),
        ("73"),
        ('/microSD_2gb/')
    ),
    (
        # MicroSD 4 GB
        (r"https://opt-list.ru/catalog/MicroSD-4-GB"),
        ("74"),
        ('/microSD_4gb/')
    ),
    (
        # MicroSD 8 GB
        (r"https://opt-list.ru/catalog/MicroSD-8-GB"),
        ("75"),
        ('/microSD_8gb/')
    ),
    (
        # MicroSD 16 GB
        (r"https://opt-list.ru/catalog/MicroSD-16-GB"),
        ("76"),
        ('/microSD_16gb/')
    ),
    (
        # MicroSD 32 GB
        (r"https://opt-list.ru/catalog/MicroSD-32-GB"),
        ("77"),
        ('/microSD_32gb/')
    ),
    (
        # MicroSD 64 GB
        (r"https://opt-list.ru/catalog/MicroSD-64-GB"),
        ("78"),
        ('/microSD_64gb/')
    ),
    (
        # Карты SD
        (r"https://opt-list.ru/catalog/SDHC-2"),
        ("79"),
        ('/SDHC/')
    ),
    (
        # Картридеры
        (r"https://opt-list.ru/catalog/Kartridery"),
        ("80"),
        ('/card_reader/')
    ),
    (
        # Разветвители USB (Хабы)
        (r"https://opt-list.ru/catalog/Kartridery-haby"),
        ("81"),
        ('/USB_hub/')
    ),
    (
        # Жесткие диски SSD
        (r"https://opt-list.ru/catalog/Zhestkie-diski-SSD"),
        ("82"),
        ('/SSD/')
    ),
    (
        # Внешние USB боксы для SSD
        (r"https://opt-list.ru/catalog/Vneshnie-USB-boksy-dlya-SSD"),
        ("83"),
        ('/HDD_box/')
    ),
    (
        # Батарейки
        (r"https://opt-list.ru/catalog/Batarejki"),
        ("87"),
        ('/battery/')
    ),
    (
        # Часовые батарейки (таблетки)
        (r"https://opt-list.ru/catalog/Chasovye"),
        ("88"),
        ('/clock_battery/')
    ),
    (
        #  Литиевые батарейки (диски)
        (r"https://opt-list.ru/catalog/Litievye"),
        ("89"),
        ('/li_ion_battery/')
    ),
    (
        # Аккумуляторы Li-Ion/Pol
        (r"https://opt-list.ru/catalog/Akkumulyatory-3"),
        ("90"),
        ('/AKB_li-ion/')
    ),
    (
        # Аккумуляторы AA, AAA, 18650, 26650
        (r"https://opt-list.ru/catalog/Akkumulyatory"),
        ("91"),
        ('/AKB_18650_AA/')
    ),
    (
        # Зарядные устройства
        (r"https://opt-list.ru/catalog/Zaryadnye-ustrojstva"),
        ("92"),
        ('/charger/')
    ),
    (
        # Аккумуляторы свинцово-кислотные
        (r"https://opt-list.ru/catalog/Akkumulyatory-svincovo-kislotnye-2"),
        ("93"),
        ('/AKB_lead_acid/')
    ),
    (
        # Супер клей
        (r"https://opt-list.ru/catalog/Super-klej"),
        ("94"),
        ('/glue/')
    ),
    (
        # Чехлы универсальные
        (r"https://opt-list.ru/catalog/Chehly-universalnye"),
        ("99"),
        ('/Universal_Cases/')
    ),
    (
        # Чехлы Olmio / Partner
        (r"https://opt-list.ru/catalog/Chehly-Olmio-Partner"),
        ("100"),
        ('/Olmio_Partner_cases/')
    ),
    (
        # Чехлы HOCO
        (r"https://opt-list.ru/catalog/Chehly-HOCO"),
        ("101"),
        ('/HOCO_cases/')
    ),
    (
        # Чехлы iPhone
        (r"https://opt-list.ru/catalog/Chehly-iPhone"),
        ("102"),
        ('/iphone_cases/')
    ),
    (
        # Чехлы Samsung
        (r"https://opt-list.ru/catalog/Chehly-Samsung"),
        ("103"),
        ('/samsung_cases/')
    ),
    (
        # Защитное стекло
        (r"https://opt-list.ru/catalog/Zashhitnoe-steklo"),
        ("105"),
        ('/protective_glass/')
    ),
    (
        # Защитная плёнка
        (r"https://opt-list.ru/catalog/Zashhitnaya-pl%D1%91nka"),
        ("106"),
        ('/protective_film/')
    ),
    (
        # Защитное стекло HOCO
        (r"https://opt-list.ru/catalog/Zashhitnoe-steklo-HOCO"),
        ("107"),
        ('/protective_HOCO/')
    ),
    (
        # Защитное стекло Smartbuy
        (r"https://opt-list.ru/catalog/Zashhitnoe-steklo-Smartbuy"),
        ("108"),
        ('/protective_Smartbuy/')
    ),
    (
        # Защитное стекло MONSTERSKIN
        (r"https://opt-list.ru/catalog/Zashhitnye-stekla-MONSTERSKIN"),
        ("109"),
        ('/protective_MONSTERSKIN/')
    ),
    (
        # Защитное стекло ОРБИТА
        (r"https://opt-list.ru/catalog/Zashhitnoe-steklo-ORBITA"),
        ("111"),
        ('/protective_ORBITA/')
    ),
    (
        # Беспроводные наушники для смартфонов
        (r"https://opt-list.ru/catalog/Besprovodnye-naushniki-dlya-smartfonov"),
        ("113"),
        ('/Wireless_headphones/')
    ),
    (
        # Вакуумные наушники (беспроводные)
        (r"https://opt-list.ru/catalog/Vakuumnye-naushniki-besprovodnye"),
        ("114"),
        ('/Wireless_headphones_vacuum/')
    ),
    (
        # Вакуумные наушники (гарнитура)
        (r"https://opt-list.ru/catalog/Vakuumnye-naushniki-garnitura"),
        ("115"),
        ('/Wireless_headphones_headset/')
    ),
    (
        # Вакуумные наушники (в пакетиках)
        (r"https://opt-list.ru/catalog/Vakuumnye-naushniki-v-paketikah-2"),
        ("116"),
        ('/Wireless_headphones_in_bags/')
    ),
    (
        # Вакуумные наушники (детские)
        (r"https://opt-list.ru/catalog/naushniki-dlya-detej"),
        ("117"),
        ('/Wireless_headphones_for_kids/')
    ),
    (
        # Apple EarPods наушники
        (r"https://opt-list.ru/catalog/Apple-naushniki"),
        ("118"),
        ('/Apple_EarPods/')
    ),
    (
        # Awei наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Awei"),
        ("119"),
        ('/headphones_Awei/')
    ),
    (
        # Defender наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Defender"),
        ("120"),
        ('/headphones_Defender/')
    ),
    (
        # Dialog наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Dialog"),
        ("121"),
        ('/headphones_Dialog/')
    ),
    (
        # ELMCOE наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/ELMCOE-naushniki-garnitury"),
        ("122"),
        ('/headphones_ELMCOE/')
    ),
    (
        # Hoco наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Hoco-naushniki-garnitury"),
        ("123"),
        ('/headphones_Hoco/')
    ),
    (
        # Inkax наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Inkax-naushniki-garnitury"),
        ("124"),
        ('/headphones_Inkax/')
    ),
    (
        # Gorsun наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Gorsun-i-dr"),
        ("125"),
        ('/headphones_Gorsun/')
    ),
    (
        # Koniycoi наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Koniycoi-naushniki-garnitury"),
        ("126"),
        ('/headphones_Koniycoi/')
    ),
    (
        # Mingge наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Mingge-naushniki-garnitury"),
        ("127"),
        ('/headphones_Mingge/')
    ),
    (
        # Monster Beats наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Monster-Beats-naushniki-garnitury"),
        ("128"),
        ('/headphones_Monster_Beats/')
    ),
    (
        # Ovleng наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Ovleng-naushniki-garnitury"),
        ("129"),
        ('/headphones_Ovleng/')
    ),
    (
        # QKZ наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/QKZ-naushniki-garnitury"),
        ("130"),
        ('/headphones_QKZ/')
    ),
    (
        # Ritmix наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Ritmix"),
        ("131"),
        ('/headphones_Ritmix/')
    ),
    (
        # Remax наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Remax"),
        ("132"),
        ('/headphones_Remax/')
    ),
    (
        # Smartbuy наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Smartbuy"),
        ("133"),
        ('/headphones_Smartbuy/')
    ),
    (
        # Wopow наушники (гарнитуры)
        (r"https://opt-list.ru/catalog/Wopow-naushniki"),
        ("134"),
        ('/headphones_Wopow/')
    ),
    (
        # Вкладыши для наушников
        (r"https://opt-list.ru/catalog/Vkladyshi-dlya-naushnikov"),
        ("135"),
        ('/Vkladyshi_dlya_naushnikov/')
    ),
    (
        # USB - Micro USB
        (r"https://opt-list.ru/catalog/USB-Micro-USB"),
        ("137"),
        ('/MicroUSB/')
    ),
    (
        # USB - iphone 5
        (r"https://opt-list.ru/catalog/USB-iphone"),
        ("138"),
        ('/iphone5/')
    ),
    (
        # USB - iphone 4
        (r"https://opt-list.ru/catalog/USB-iphone-2"),
        ("139"),
        ('/iphone4/')
    ),
    (
        # USB - Type-C
        (r"https://opt-list.ru/catalog/USB-Type-C-Xiaomi-Meizu"),
        ("140"),
        ('/TypeC/')
    ),
    (
        # USB - Samsung Tab
        (r"https://opt-list.ru/catalog/USB-Samsung-Tab-2"),
        ("141"),
        ('/Samsung_Tab/')
    ),
    (
        # USB - mini USB
        (r"https://opt-list.ru/catalog/USB-mini-USB"),
        ("142"),
        ('/miniUSB/')
    ),
    (
        # Несколько шнуров в одном (метелки)
        (r"https://opt-list.ru/catalog/Neskolko-shnurov-v-odnom-metelki"),
        ("143"),
        ('/USB_all_in_one/')
    ),
    (
        # Переходник для шнуров
        (r"https://opt-list.ru/catalog/Perehodnik"),
        ("144"),
        ('/USB_cable_adapter/')
    ),
    (
        # Вилки USB
        (r"https://opt-list.ru/catalog/Vilki-USB"),
        ("145"),
        ('/220_USB_charger/')
    ),
    (
        # Сетевые зарядные СЗУ
        (r"https://opt-list.ru/catalog/SZU-AZU"),
        ("146"),
        ('/220_USB_charger_with_cable/')
    ),
    (
        # Беспроводное зарядное устройство
        (r"https://opt-list.ru/catalog/Besprovodnoe-zaryadnoe-ustrojstvo"),
        ("147"),
        ('/wireless_charger/')
    ),
    (
        # Лягушка
        (r"https://opt-list.ru/catalog/Lyagushka"),
        ("148"),
        ('/frog/')
    ),
    (
        # Автомобильное зарядное устройство АЗУ
        (r"https://opt-list.ru/catalog/Avtomobilnoe-zaryadnoe-ustrojstvo-optom"),
        ("149"),
        ('/AZU/')
    ),
    (
        # Держатели телефонов и планшетов
        (r"https://opt-list.ru/catalog/Derzhaleli-telefonov-i-planshetov"),
        ("150"),
        ('/telephone_holders/')
    ),
    (
        # Держатели для телефона напольные
        (r"https://opt-list.ru/catalog/Derzhateli-dlya-telefona-napolnye"),
        ("151"),
        ('/Holders_for_phone_floor/')
    ),
    (
        # Портативные аккумуляторы
        (r"https://opt-list.ru/catalog/Portativnye-akkumulyatory"),
        ("152"),
        ('/powerbank/')
    ),
    (
        # Bluetooth гарнитура
        (r"https://opt-list.ru/catalog/Aksessuary"),
        ("153"),
        ('/Bluetooth_headset/')
    ),
    (
        # Монопод (палки для сэлфи)
        (r"https://opt-list.ru/catalog/Palka-dlya-selfi"),
        ("154"),
        ('/monopod_selfie/')
    ),
    (
        # Набор линз для смартфонов
        (r"https://opt-list.ru/catalog/Nabor-linz-dlya-smartfonov"),
        ("155"),
        ('/linz/')
    ),
    (
        # Очки виртуальной реальности VR Box
        (r"https://opt-list.ru/catalog/Aksessuary-dlya-smartfonov-2"),
        ("156"),
        ('/VR_Box/')
    ),
    (
        # Аккумулятор для телефонов
        (r"https://opt-list.ru/catalog/Akkumulyator-dlya-telefonov"),
        ("159"),
        ('/AKB_for_telephone/')
    ),
    (
        # Прочее
        (r"https://opt-list.ru/catalog/Prochee-6"),
        ("160"),
        ('/mobile_other/')
    ),
    (
        # Фотобоксы
        (r"https://opt-list.ru/catalog/Fotoboksy"),
        ("161"),
        ('/photoBOX/')
    ),
    (
        # Цифровой ресивер
        (r"https://opt-list.ru/catalog/Cifrovoj-resiver-portativnyj-DVD"),
        ("165"),
        ('/Digital_receiver/')
    ),
    (
        # Кронштейн для телевизора
        (r"https://opt-list.ru/catalog/Kronshtejn-dlya-televizora"),
        ("166"),
        ('/TV_mount/')
    ),
    (
        # Антенны, усилители, блоки питания
        (r"https://opt-list.ru/catalog/Antenny-usiliteli-bloki-pitaniya"),
        ("167"),
        ('/TV_other/')
    ),
    (
        # ТВ пульты универсальные
        (r"https://opt-list.ru/catalog/TV-pulty-universalnye"),
        ("168"),
        ('/TV_remotes/')
    ),
    (
        # GSM репитеры, антенны
        (r"https://opt-list.ru/catalog/GSM-repitery-antenny"),
        ("169"),
        ('/GSM_repeater/')
    ),
    (
        # Телевизоры
        (r"https://opt-list.ru/catalog/Televizory"),
        ("170"),
        ('/TV/')
    ),
    (
        # Беспроводная мышь
        (r"https://opt-list.ru/catalog/Besprovodnaya-mysh"),
        ("175"),
        ('/wireless_mouse/')
    ),
    (
        # Проводная мышь
        (r"https://opt-list.ru/catalog/Provodnaya-mysh"),
        ("176"),
        ('/mouse/')
    ),
    (
        # Игровая мышь
        (r"https://opt-list.ru/catalog/Igrovaya-mysh"),
        ("177"),
        ('/game_mouse/')
    ),
    (
        # Коврики для мышки
        (r"https://opt-list.ru/catalog/Kovriki-dlya-myshki"),
        ("178"),
        ('/mouse_pads/')
    ),
    (
        # Комплекты клавиатура мышь
        (r"https://opt-list.ru/catalog/Komplekty"),
        ("179"),
        ('/mouse_and_keyboard/')
    ),
    (
        # Клавиатуры
        (r"https://opt-list.ru/catalog/Klaviatury"),
        ("180"),
        ('/keyboard/')
    ),
    (
        # Web-камера
        (r"https://opt-list.ru/catalog/Web-kamera"),
        ("181"),
        ('/web_camera/')
    ),
    (
        # Микрофоны для ПК
        (r"https://opt-list.ru/catalog/Mikrofony-2"),
        ("180"),
        ('/microphone_for_PC/')
    ),
    (
        # Внешние USB боксы для накопителей HDD, SSD
        (r"https://opt-list.ru/catalog/Vneshnie-USB-boksy-dlya-nakopitelej-HDD-SSD-2"),
        ("183"),
        ('/out_usb_box_for_HDD/')
    ),
    (
        # Геймпады ПК Консоли
        (r"https://opt-list.ru/catalog/Gejmpady"),
        ("184"),
        ('/gamepad_PC_Console/')
    ),
    (
        # Блок питания для ПК
        (r"https://opt-list.ru/catalog/Blok-pitaniya-dlya-PK"),
        ("185"),
        ('/power_block_PC/')
    ),
    (
        # Рули игровые для ПК
        (r"https://opt-list.ru/catalog/Ruli"),
        ("186"),
        ('/Game_wheels_for_PC/')
    ),
    (
        # Вентиляторы охлаждения (кулеры)
        (r"https://opt-list.ru/catalog/Ventilyatory-ohlazhdeniya-kulery"),
        ("187"),
        ('/cooler_PC/')
    ),
    (
        # Мониторные наушники
        (r"https://opt-list.ru/catalog/Monitornye-naushniki-3"),
        ("189"),
        ('/Monitor_headphones_PC/')
    ),
    (
        # Мониторные наушники с микрофоном
        (r"https://opt-list.ru/catalog/Monitornye"),
        ("190"),
        ('/Monitor_headphones__with_Micro_PC/')
    ),
    (
        # Беспроводные наушники и другие
        (r"https://opt-list.ru/catalog/Besprovodnye-naushniki"),
        ("191"),
        ('/wireless_hedphones_and_other/')
    ),
    (
        # Микрофоны
        (r"https://opt-list.ru/catalog/Mikrofony"),
        ("192"),
        ('/microphone_PC/')
    ),
    (
        # Активные колонки 2.0
        (r"https://opt-list.ru/catalog/Aktivnye-kolonki-2-0"),
        ("193"),
        ('/Active_speakers_2-0/')
    ),
    (
        # Акустические системы 2.1 / 5.1
        (r"https://opt-list.ru/catalog/Akusticheskie-sistemy-2-1-5"),
        ("194"),
        ('/Active_speakers_2-0_5-1/')
    ),
    (
        # Сетевые удлинители (фильтр)
        (r"https://opt-list.ru/catalog/Setevye-udliniteli-filtr"),
        ("195"),
        ('/Network_extension_cords/')
    ),
    (
        # Блок питания (универсальные)
        (r"https://opt-list.ru/catalog/Blok-pitaniya-dlya-noutbukov"),
        ("195"),
        ('/power_block_universal/')
    ),
    (
        # Подставки для ноутбука
        (r"https://opt-list.ru/catalog/Podstavki-dlya-noutbuka"),
        ("197"),
        ('/coooler_for_notebook/')
    ),
    (
        # Столик для ноутбука
        (r"https://opt-list.ru/catalog/Stolik-dlya-noutbuka"),
        ("198"),
        ('/table_for_notebook/')
    ),
    (
        # Сумки для ноутбуков, планшетов
        (r"https://opt-list.ru/catalog/Sumki-dlya-noutbukov"),
        ("199"),
        ('/bags_for_notebook_table/')
    ),
    (
        # Чистящие средства
        (r"https://opt-list.ru/catalog/Chistyashhie-salfetki"),
        ("200"),
        ('/cleaners_PC/')
    ),
    (
        # USB TВ Тюнер
        (r"https://opt-list.ru/catalog/USB-TV-Tyuner"),
        ("201"),
        ('/usb_TV_tuner/')
    ),
    (
        # Маршрутизаторы Wi-Fi
        (r"https://opt-list.ru/catalog/Marshrutizatory-Wi-Fi"),
        ("202"),
        ('/WIFI/')
    ),
    (
        # Термопасты
        (r"https://opt-list.ru/catalog/Termopasty-2"),
        ("203"),
        ('/Thermal_paste/')
    ),
    (
        # Гаджеты USB
        (r"https://opt-list.ru/catalog/Gadzhet"),
        ("204"),
        ('/USB_gadgets/')
    ),
    (
        # Офисное кресло
        (r"https://opt-list.ru/catalog/Ofisnoe-kreslo"),
        ("205"),
        ('/Office_chair/')
    ),
    (
        # 4G модемы
        (r"https://opt-list.ru/catalog/4G-modemy"),
        ("206"),
        ('/4G_modem/')
    ),
    (
        # Райзер для видеокарт
        (r"https://opt-list.ru/catalog/Rajzer-dlya-videokart"),
        ("207"),
        ('/razer_for_videocard/')
    ),
    (
        # Портативные колонки
        (r"https://opt-list.ru/catalog/Prochie-2"),
        ("211"),
        ('/portative_speaker/')
    ),
    (
        # Портативные колонки HOCO
        (r"https://opt-list.ru/catalog/Portativnye-kolonki-HOCO"),
        ("212"),
        ('/portative_speaker_HOCO/')
    ),
    (
        # Портативные колонки с подсветкой
        (r"https://opt-list.ru/catalog/Disko-LED-s-proekciej"),
        ("213"),
        ('/portative_speaker_with_light/')
    ),
    (
        # Напольная акустика
        (r"https://opt-list.ru/catalog/Napolnaya-akustika"),
        ("214"),
        ('/floor_speaker/')
    ),
    (
        # Диктофоны
        (r"https://opt-list.ru/catalog/Diktofony"),
        ("215"),
        ('/Voice_recorders/')
    ),
    (
        # Усилитель звука
        (r"https://opt-list.ru/catalog/Usilitel-zvuka"),
        ("216"),
        ('/Sound_amplifier/')
    ),
    (
        # Модули MP3
        (r"https://opt-list.ru/catalog/Moduli-MP3"),
        ("217"),
        ('/MP3_module/')
    ),
    (
        # Саундбары
        (r"https://opt-list.ru/catalog/Saundbary"),
        ("218"),
        ('/soundBAR/')
    ),
    (
        # Радиоприемники (220V)
        (r"https://opt-list.ru/catalog/Radiopriemniki"),
        ("222"),
        ('/Radio_receivers/')
    ),
    (
        # Радиоприемники (3В,9В,12В)
        (r"https://opt-list.ru/catalog/Radiopriemniki-3V-9V"),
        ("223"),
        ('/Radio_receivers_3v-9v/')
    ),
    (
        # Радиоприёмники (USB, SD)
        (r"https://opt-list.ru/catalog/Radiopri%D1%91mniki-USB-SD"),
        ("224"),
        ('/Radio_receivers_USB_SD/')
    ),
    (
        # Радиоприемники Atlanfa
        (r"https://opt-list.ru/catalog/Atlanfa-2"),
        ("225"),
        ('/Radio_receivers_Atlanfa/')
    ),
    (
        # MP3-плееры и диктофоны
        (r"https://opt-list.ru/catalog/MP3-pleera"),
        ("227"),
        ('/MP3player_VOICErecorder/')
    ),
    (
        # Автоаксессуары (алкотестеры, часы, модули и тд)
        (r"https://opt-list.ru/catalog/Avtoaksessuary"),
        ("130"),
        ('/auto_acsess/')
    ),
    (
        # Авто сканеры и OBD
        (r"https://opt-list.ru/catalog/Avto-skanery-i-OBD"),
        ("231"),
        ('/auto_scanner/')
    ),
    (
        # Авто пусковые и зарядные устройства
        (r"https://opt-list.ru/catalog/Avto-Puskovoe-zaryadnoe-ustrojstvo-High-power"),
        ("232"),
        ('/auto_starter/')
    ),
    (
        # Автомобильное зарядное устройство (USB)
        (r"https://opt-list.ru/catalog/Avtomobilnoe-zaryadnoe-ustrojstvo-USB"),
        ("233"),
        ('/auto_charger_next/')
    ),
    (
        # Автолапмы
        (r"https://opt-list.ru/catalog/Avtolapmy"),
        ("234"),
        ('/auto_light/')
    ),
    (
        # Автомагнитолы
        (r"https://opt-list.ru/catalog/Avtomagnitoly"),
        ("235"),
        ('/car_player/')
    ),
    (
        # Автосигнализация
        (r"https://opt-list.ru/catalog/Avtosignalizaciya-2"),
        ("236"),
        ('/auto_alarm/')
    ),
    (
        # Авто Видеорегистраторы
        (r"https://opt-list.ru/catalog/Avto-Videoregistratory"),
        ("237"),
        ('/auto_recorder/')
    ),
    (
        # Брелок для ключей
        (r"https://opt-list.ru/catalog/Brelok-2"),
        ("238"),
        ('/auto_for_keys/')
    ),
    (
        # Брелок для сигнализации
        (r"https://opt-list.ru/catalog/Brelok-dlya-signalizacii"),
        ("239"),
        ('/Keychain_for_alarm/')
    ),
    (
        # Дневные ходовые огни
        (r"https://opt-list.ru/catalog/Dnevnye-hodovye-ogni"),
        ("240"),
        ('/Daytime_Running_Lights/')
    ),
    (
        # Держатели для телефона, планшета
        (r"https://opt-list.ru/catalog/Derzhateli-dlya-telefona-plansheta"),
        ("241"),
        ('/auto_holder_2/')
    ),
    (
        # Комплекты проводов для аккустики
        (r"https://opt-list.ru/catalog/Komplekty-provodov-dlya-akkustiki"),
        ("242"),
        ('/auto_audio_cable/')
    ),
    (
        # Колонки автомобильные
        (r"https://opt-list.ru/catalog/Kolonki-avtomobilnye"),
        ("243"),
        ('/auto_speaker/')
    ),
    (
        # Коврики и органайзеры
        (r"https://opt-list.ru/catalog/Kovriki"),
        ("244"),
        ('/auto_Rugs_and_organizers/')
    ),
    (
        # Крепление на стекло
        (r"https://opt-list.ru/catalog/Kreplenie-na-steklo"),
        ("245"),
        ('/holder_on_glass/')
    ),
    (
        # Модуляторы FM МР3
        (r"https://opt-list.ru/catalog/Modulyatory-MR3"),
        ("246"),
        ('/auto_FM_МР3/')
    ),
    (
        # Мониторы, видеокамеры
        (r"https://opt-list.ru/catalog/Monitory"),
        ("247"),
        ('/auto_monitor_camer/')
    ),
    (
        # Радар-детекторы
        (r"https://opt-list.ru/catalog/Radar-detektory"),
        ("249"),
        ('/radar-detect/')
    ),
    (
        # Разветвители в прикуриватель
        (r"https://opt-list.ru/catalog/Razvetviteli"),
        ("250"),
        ('/Splitter_to_cigarette_lighter/')
    ),
    (
        # Система парковки
        (r"https://opt-list.ru/catalog/Sistema-parkovki"),
        ("251"),
        ('/parking_system/')
    ),
    (
        # Стеклоподъемники
        (r"https://opt-list.ru/catalog/Steklopodjemniki"),
        ("252"),
        ('/glass_uppers/')
    ),
    (
        # Центральный замок
        (r"https://opt-list.ru/catalog/Centralnyj-zamok"),
        ("254"),
        ('/central_lock/')
    ),
    (
        # Чехол для брелка автосигнализации
        (r"https://opt-list.ru/catalog/Chehol-dlya-brelka-avtosignalizacii"),
        ("255"),
        ('/Cover_for_remote_control_car_alarm/')
    ),
    (
        # Рации и антены
        (r"https://opt-list.ru/catalog/Racii-i-anteny"),
        ("256"),
        ('/Radio_and_antennas/')
    ),
    (
        # Переходники и штекеры
        (r"https://opt-list.ru/catalog/Perehodniki-i-shtekery"),
        ("260"),
        ('/adapter_other_2/')
    ),
    (
        # AUX
        (r"https://opt-list.ru/catalog/AUX"),
        ("261"),
        ('/AUX_2/')
    ),
    (
        # Удлинитель наушников
        (r"https://opt-list.ru/catalog/Udlinitel-naushnikov"),
        ("262"),
        ('/Headphone_extension/')
    ),
    (
        # JACK 3,5-RCA
        (r"https://opt-list.ru/catalog/Audioshnury-perehodniki-shtekera"),
        ("263"),
        ('/JACK3-5_RCA/')
    ),
    (
        # RCA-RCA
        (r"https://opt-list.ru/catalog/Videoshnury-RCA-RCA"),
        ("264"),
        ('/RCA_RCA/')
    ),
    (
        # SCART
        (r"https://opt-list.ru/catalog/Videoshnury-s-razjemom-SCART"),
        ("265"),
        ('/SCART/')
    ),
    (
        # HDMI
        (r"https://opt-list.ru/catalog/HDMI"),
        ("266"),
        ('/HDMI/')
    ),
    (
        # VGA/DVI
        (r"https://opt-list.ru/catalog/VGA-DVI"),
        ("267"),
        ('/VGA_DVI/')
    ),
    (
        # HDMI/DVI/VGA переходники
        (r"https://opt-list.ru/catalog/HDMI-DVI-VGA-perehodniki"),
        ("268"),
        ('/HDMI_DVI_VGA_adapter/')
    ),
    (
        # Кабель для принтера
        (r"https://opt-list.ru/catalog/Kabel-dlya-printera"),
        ("269"),
        ('/cable_for_printer/')
    ),
    (
        # Удлинитель USB
        (r"https://opt-list.ru/catalog/Udlinitel-USB"),
        ("270"),
        ('/USB_extension/')
    ),
    (
        # Патчкорд интернет кабель
        (r"https://opt-list.ru/catalog/Patchkord"),
        ("271"),
        ('/patch_cord/')
    ),
    (
        # Сетевой шнур питания
        (r"https://opt-list.ru/catalog/Setevoj-shnur"),
        ("272"),
        ('/power_cable/')
    ),
    (
        # Телефонный удлинитель
        (r"https://opt-list.ru/catalog/Telefonnyj-udlinitel"),
        ("273"),
        ('/telephone_extension/')
    ),
    (
        # ТВ переходники
        (r"https://opt-list.ru/catalog/TV-SAT-kraby"),
        ("274"),
        ('/TV_adapter/')
    ),
    (
        # ТВ, антенный кабель
        (r"https://opt-list.ru/catalog/TV-kabel"),
        ("275"),
        ('/TV_cable/')
    ),
    (
        # Прочее
        (r"https://opt-list.ru/catalog/Prochee-5"),
        ("276"),
        ('/TV_other/')
    ),
    (
        # Bluetooth адаптеры
        (r"https://opt-list.ru/catalog/Bluetooth-adaptery"),
        ("277"),
        ('/Bluetooth_USB/')
    ),
    (
        # Кабель оптический
        (r"https://opt-list.ru/catalog/Kabel-opticheskij"),
        ("278"),
        ('/cable_optical/')
    ),
    (
        # Адаптеры питания
        (r"https://opt-list.ru/catalog/Adapter-pitaniya"),
        ("280"),
        ('/power_adapter/')
    ),
    (
        # DVD-проигрыватель
        (r"https://opt-list.ru/catalog/DVD-proigryvatel"),
        ("283"),
        ('/portable_DVD/')
    ),
    (
        # Планшеты
        (r"https://opt-list.ru/catalog/Planshety"),
        ("284"),
        ('/tablet/')
    ),
    (
        # Мобильные телефоны
        (r"https://opt-list.ru/catalog/Mobilnye-telefony"),
        ("285"),
        ('/mobile_phone/')
    ),
    (
        # Портативные ТВ
        (r"https://opt-list.ru/catalog/Portativnye-TV"),
        ("286"),
        ('/portable_TV/')
    ),
    (
        # НАРУЧНЫЕ ЧАСЫ
        (r"https://opt-list.ru/catalog/NARUChNYe-ChASY-2"),
        ("290"),
        ('/hand_clock/')
    ),
    (
        # SMART ЧАСЫ
        (r"https://opt-list.ru/catalog/SMART-ChASY-2"),
        ("291"),
        ('/smart_clock/')
    ),
    (
        # БОКС ДЛЯ ЧАСОВ
        (r"https://opt-list.ru/catalog/BOKS-DLya-ChASOV"),
        ("292"),
        ('/box_for_clock/')
    ),
    (
        # Запчасти для светодиодных фонарей
        (r"https://opt-list.ru/catalog/Zapchasti-dlya-svetodiodnyh-fonarej"),
        ("297"),
        ('/repir_to_flashlight/')
    ),
    (
        # Фонари аккумуляторные
        (r"https://opt-list.ru/catalog/Fonari-akkumulyatornye"),
        ("298"),
        ('/flashlight_with_AKB/')
    ),
    (
        # Фонари велосипедные
        (r"https://opt-list.ru/catalog/Fonari-velosipednye"),
        ("299"),
        ('/flashlight_velocity/')
    ),
    (
        # Фонари налобные св/диодные
        (r"https://opt-list.ru/catalog/Fonari-nalobnye-sv-diodnye"),
        ("301"),
        ('/flashlight_on_head/')
    ),
    (
        # Фонари налобные св/диодные 1 LED
        (r"https://opt-list.ru/catalog/Fonari-nalobnye-sv-diodnye-1-LED"),
        ("302"),
        ('/flashlight_on_head_1_LED/')
    ),
    (
        # Фонари налобные св/диодные по 0,5W
        (r"https://opt-list.ru/catalog/Fonari-nalobnye-sv-diodnye-po-0"),
        ("303"),
        ('/flashlight_on_head_05_LED/')
    ),
    (
        # Фонари Патриот, BORUIT
        (r"https://opt-list.ru/catalog/Fonari-Patriot"),
        ("304"),
        ('/flashlight_patriot/')
    ),
    (
        # Фонари ручные св/диодные
        (r"https://opt-list.ru/catalog/Fonari-ruchnye-sv-diodnye"),
        ("305"),
        ('/handless_flashlights/')
    ),
    (
        # Фонари ручные св/диодные по 0,5W
        (r"https://opt-list.ru/catalog/Fonari-ruchnye-sv-diodnye-po-0"),
        ("306"),
        ('/handless_flashlights_05/')
    ),
    (
        # Фонари ручные св/диодные 1 LED
        (r"https://opt-list.ru/catalog/Fonari-ruchnye-sv-diodnye-1-LED"),
        ("307"),
        ('/handless_flashlights_1/')
    ),
    (
        # Фонари и лампы Smartbuy
        (r"https://opt-list.ru/catalog/Smartbuy-2"),
        ("308"),
        ('/flashlights_Smartbuy/')
    ),
    (
        # Брелки, Лазерные указки
        (r"https://opt-list.ru/catalog/Fonarik-Brelok"),
        ("309"),
        ('/hand_laser/')
    ),
    (
        # LED лента и доп. материалы
        (r"https://opt-list.ru/catalog/LED-lenta"),
        ("310"),
        ('/LED_tape_and_other/')
    ),
    (
        # LED светильники и переходники
        (r"https://opt-list.ru/catalog/LED-svetilniki"),
        ("311"),
        ('/LED_lights/')
    ),
    (
        # LED лампочки
        (r"https://opt-list.ru/catalog/LED-lampa"),
        ("312"),
        ('/LED_light_bulbs/')
    ),
    (
        # Лазерные установки
        (r"https://opt-list.ru/catalog/Lazernye-ustanovki"),
        ("313"),
        ('/Laser_systems/')
    ),
    (
        # Световые установки
        (r"https://opt-list.ru/catalog/Svetovye-ustanovki"),
        ("314"),
        ('/light_system/')
    ),
    (
        # Стикеры светоотражающие
        (r"https://opt-list.ru/catalog/Svetootrazhayushhie-LED-stikery"),
        ("315"),
        ('/Reflective_Stickers/')
    ),
    (
        # Стробоскопы
        (r"https://opt-list.ru/catalog/Stroboskopy"),
        ("316"),
        ('/Strobe_light/')
    ),
    (
        # LED свечи, ёлки
        (r"https://opt-list.ru/catalog/LED-svechi-%D1%91lki"),
        ("317"),
        ('/LED_candles/')
    ),
    (
        # Ночники
        (r"https://opt-list.ru/catalog/Nochniki"),
        ("318"),
        ('/Night_Lights/')
    ),
    (
        # Гирлянды
        (r"https://opt-list.ru/catalog/Girlyandy"),
        ("319"),
        ('/Garlands/')
    ),
    (
        # Геймпады
        (r"https://opt-list.ru/catalog/Gejmpady-2"),
        ("323"),
        ('/gamepad/')
    ),
    (
        # Зарядные станции и шнуры для зарядки
        (r"https://opt-list.ru/catalog/Zaryadnye-stancii-dlya-PS"),
        ("324"),
        ('/console_accessory/')
    ),
    (
        # Насадки на стики и др.
        (r"https://opt-list.ru/catalog/Nasadki-na-stiki-i-dr-2"),
        ("325"),
        ('/Tips_on_sticks/')
    ),
    (
        # Чехол для джойстика PS 4
        (r"https://opt-list.ru/catalog/Chehol-dlya-dzhojstika-PS"),
        ("326"),
        ('/JoystickPS4_Case/')
    ),
    (
        # Наклейка виниловая для PS 4
        (r"https://opt-list.ru/catalog/PS-4-Naklejka-vinilovaya"),
        ("327"),
        ('/vinil_on_ps4/')
    ),
    (
        # Гироскутеры
        (r"https://opt-list.ru/catalog/Giroskuter-hit-sezona"),
        ("329"),
        ('/Giroscooters/')
    ),
    (
        # Металлодетектор
        (r"https://opt-list.ru/catalog/Metallodetektor"),
        ("332"),
        ('/metal_detect/')
    ),
    (
        # Экшн камеры
        (r"https://opt-list.ru/catalog/Jekshn-kamery"),
        ("333"),
        ('/action_camera/')
    ),
    (
        # IP Камеры
        (r"https://opt-list.ru/catalog/IP-Kamery"),
        ("334"),
        ('/ip_camera/')
    ),
    (
        # HD-AHD Камеры
        (r"https://opt-list.ru/catalog/HD-AHD-Kamery"),
        ("335"),
        ('/HD_AH_camera/')
    ),
    (
        # IP камеры DAHUA
        (r"https://opt-list.ru/catalog/IP-kamery-DAHUA"),
        ("336"),
        ('/DAHUA_camera/')
    ),
    (
        # Аналоговые камеры (ССTV)
        (r"https://opt-list.ru/catalog/Analogovye-kamery-SSTV"),
        ("337"),
        ('/analog_camera/')
    ),
    (
        # IP камеры HIKVISION
        (r"https://opt-list.ru/catalog/IP-kamery-HIKVISION"),
        ("338"),
        ('/HIKVISION_camera/')
    ),
    (
        # Видеорегистраторы
        (r"https://opt-list.ru/catalog/Videoregistratory"),
        ("339"),
        ('/video_registrator_for_camera/')
    ),
    (
        # Комплекты видеонаблюдения
        (r"https://opt-list.ru/catalog/Komplekty-videonablyudeniya"),
        ("340"),
        ('/CCTV_Kits/')
    ),
    (
        # Домофоны, видеоняни, сигнализации
        (r"https://opt-list.ru/catalog/Domofony-videonyani-signalizacii"),
        ("341"),
        ('/Doorphones_baby_monitors_alarms/')
    ),
    (
        # Удлинители для в/камер
        (r"https://opt-list.ru/catalog/Udliniteli-dlya-v-kamer"),
        ("343"),
        ('/camera_extension/')
    ),
    (
        # Прочее
        (r"https://opt-list.ru/catalog/Prochee-2"),
        ("344"),
        ('/camera_other/')
    ),
    (
        # Игровые приставки Dendy (Денди)
        (r"https://opt-list.ru/catalog/Dendy-Dendi-2"),
        ("348"),
        ('/Dendy/')
    ),
    (
        # Игровые приставки Sega (Сега)
        (r"https://opt-list.ru/catalog/Sega-Sega-2"),
        ("349"),
        ('/Sega/')
    ),
    (
        # Игровые приставки Hamy (2в1 Sega+Dendy)
        (r"https://opt-list.ru/catalog/Dendy-Sega-2in1-HAMY-2"),
        ("350"),
        ('/Hamy/')
    ),
    (
        # Аксессуары для Dendy, Sega, Hamy
        (r"https://opt-list.ru/catalog/Aksessuary-Dendy-Sega-2"),
        ("351"),
        ('/accessopries_on_HAMY_other/')
    ),
    (
        # Картриджи для приставки Sega (Сега)
        (r"https://opt-list.ru/catalog/Katridzh-Sega"),
        ("352"),
        ('/game_for_sega/')
    ),
    (
        # Картриджи для приставки Dendy (Денди)
        (r"https://opt-list.ru/catalog/Katridzh-dendi"),
        ("353"),
        ('/game_for_Dendy/')
    ),
    (
        # Тетрисы, тамагочи и Кубик Рубика
        (r"https://opt-list.ru/catalog/Tetrisy-i-tamagochi"),
        ("354"),
        ('/tamagochi/')
    ),
    (
        # Машинки для стрижки
        (r"https://opt-list.ru/catalog/Mashinki-dlya-strizhki-plojki"),
        ("359"),
        ('/head_trimmers/')
    ),
    (
        # Плойки
        (r"https://opt-list.ru/catalog/Plojki"),
        ("360"),
        ('/Curling/')
    ),
    (
        # Бритвы
        (r"https://opt-list.ru/catalog/Otechestvennye-britvy"),
        ("361"),
        ('/Razors/')
    ),
    (
        # Комплектующие для электробритв
        (r"https://opt-list.ru/catalog/Komplektuyushhie-dlya-jelektrobritv"),
        ("362"),
        ('/for_Razors/')
    ),
    (
        # Триммеры
        (r"https://opt-list.ru/catalog/Trimmery"),
        ("363"),
        ('/trimmers/')
    ),
    (
        # Фен
        (r"https://opt-list.ru/catalog/Fen"),
        ("364"),
        ('/Hair_dryer/')
    ),
    (
        # Усилители звука
        (r"https://opt-list.ru/catalog/Usiliteli-zvuka"),
        ("365"),
        ('/Hearing_aid/')
    ),
    (
        # Массажеры
        (r"https://opt-list.ru/catalog/Massazhery"),
        ("366"),
        ('/Massagers/')
    ),
    (
        # Прочее
        (r"https://opt-list.ru/catalog/Prochee-4"),
        ("367"),
        ('/cute_other/')
    ),
    (
        # Товары для животных
        (r"https://opt-list.ru/catalog/Tovary-dlya-zhivotnyh"),
        ("369"),
        ('/for_animal/')
    ),
    (
        # Ножи 1
        (r"https://opt-list.ru/catalog/Nozhi-Patriort"),
        ("372"),
        ('/knife/')
    ),
    (
        # Ножи 2
        (r"https://opt-list.ru/catalog/Nozhi-s-instrumentami"),
        ("372"),
        ('/knife/')
    ),
    (
        #Ножи 3
        (r"https://opt-list.ru/catalog/Nozhi-Sledopyt"),
        ("372"),
        ('/knife/')
    ),
    (
        # Бинокли
        (r"https://opt-list.ru/catalog/Binokli"),
        ("373"),
        ('/Binoculars/')
    ),
    (
        # Рации
        (r"https://opt-list.ru/catalog/Racii"),
        ("374"),
        ('/Walkie_talkies/')
    ),
    (
        # Прицелы
        (r"https://opt-list.ru/catalog/Pricely"),
        ("375"),
        ('/Sights/')
    ),
    (
        # Шокер
        (r"https://opt-list.ru/catalog/Shoker"),
        ("376"),
        ('/Shoker/')
    ),
    (
        # Прочее туризм
        (r"https://opt-list.ru/catalog/Prochee-8"),
        ("377"),
        ('/tourism_other/')
    ),
    (
        # Звонки дверные
        (r"https://opt-list.ru/catalog/Zvonki-dvernye"),
        ("381"),
        ('/door_alarm/')
    ),
    (
        # часы  3V, Прочие
        (r"https://opt-list.ru/catalog/3V-Prochie"),
        ("383"),
        ('/clock_3V/')
    ),
    (
        # Часы 220V
        (r"https://opt-list.ru/catalog/Chasy"),
        ("384"),
        ('/clock_220V/')
    ),
    (
        # Часы автомобильные
        (r"https://opt-list.ru/catalog/Chasy-avtomobilnye"),
        ("385"),
        ('/clock_auto/')
    ),
    (
        # Проводные телефоны
        (r"https://opt-list.ru/catalog/Provodnye-telefony"),
        ("388"),
        ('/cable_home_phone/')
    ),
    (
        # Радиотелефоны
        (r"https://opt-list.ru/catalog/Radiotelefony"),
        ("389"),
        ('/home_radio_phone/')
    ),
    (
        # Лупы
        (r"https://opt-list.ru/catalog/Lupy"),
        ("390"),
        ('/Loupes/')
    ),
    (
        # Третья рука
        (r"https://opt-list.ru/catalog/Tretya-ruka"),
        ("392"),
        ('/third_hand/')
    ),
    (
        # Длинногубцы, кусачки
        (r"https://opt-list.ru/catalog/Dlinnogubcy-kusachki"),
        ("393"),
        ('/pliers/')
    ),
    (
        # Набор отверток
        (r"https://opt-list.ru/catalog/Nabor-otvertok"),
        ("394"),
        ('/screwdrivers/')
    ),
    (
        # Паяльник
        (r"https://opt-list.ru/catalog/Payalnik"),
        ("395"),
        ('/Soldering_iron/')
    ),
    (
        # Пинцет
        (r"https://opt-list.ru/catalog/Pincet"),
        ("396"),
        ('/Pincet/')
    ),
    (
        # Клеевой пистолет
        (r"https://opt-list.ru/catalog/Kleevoj-pistolet"),
        ("397"),
        ('/Glue_gun/')
    ),
    (
        # Прочее для дома и офиса
        (r"https://opt-list.ru/catalog/Prochee-7"),
        ("398"),
        ('/for_home_other/')
    ),
    (
        # Штангенциркули
        (r"https://opt-list.ru/catalog/Shtangencirkuli"),
        ("399"),
        ('/Calipers/')
    ),
    (
        # Клей, изолента, термоскотч, всё для пайки
        (r"https://opt-list.ru/catalog/Klej-izolenta-termoskotch"),
        ("400"),
        ('/Glue_and_other/')
    ),
    (
        # Изделия для сети 220В
        (r"https://opt-list.ru/catalog/Adaptery-vilki-trojniki"),
        ("401"),
        ('/for_220W/')
    ),
    (
        # Сетевые удлинители
        (r"https://opt-list.ru/catalog/Setevye-udliniteli"),
        ("402"),
        ('/220W_extension/')
    ),
    (
        # Мультиметры
        (r"https://opt-list.ru/catalog/Multimetry"),
        ("403"),
        ('/Multimeters/')
    ),
    (
        # Переносной Светильник
        (r"https://opt-list.ru/catalog/Perenosnoj-Svetilnik"),
        ("404"),
        ('/portable_air_light/')
    ),
    (
        # Пьезозажигалка
        (r"https://opt-list.ru/catalog/Pezozazhigalka"),
        ("405"),
        ('/Piezo_lighter/')
    ),
    (
        # Прочее AGAIN&!&!&!
        (r"https://opt-list.ru/catalog/Prochee-3"),
        ("406"),
        ('/other_again/')
    ),
    (
        # Розетка, вилка - адаптер
        (r"https://opt-list.ru/catalog/Rozetka-vilka-adapter"),
        ("407"),
        ('/220W_adapter/')
    ),
    (
        # Розетки и выключатели
        (r"https://opt-list.ru/catalog/Rozetki-i-vyklyuchateli"),
        ("408"),
        ('/220W_ON_OFF/')
    ),
    (
        # Солнечная панель
        (r"https://opt-list.ru/catalog/Solnechnaya-panel"),
        ("409"),
        ('/light_panel/')
    ),
    (
        # Весы портативные, безмены
        (r"https://opt-list.ru/catalog/Vesy-portativnye-bezmeny-2"),
        ("410"),
        ('/Libra/')
    ),
    (
        # Бокс для радиодеталей
        (r"https://opt-list.ru/catalog/Boks-dlya-radiodetalej"),
        ("411"),
        ('/box_for_detail/')
    ),
    (
        # Калькуляторы
        (r"https://opt-list.ru/catalog/Kalkulyatory"),
        ("412"),
        ('/calc/')
    ),
    (
        # Детектор для проверки денег
        (r"https://opt-list.ru/catalog/Detektor-dlya-proverki-deneg"),
        ("413"),
        ('/money_detector/')
    ),
    (
        # Платы Arduino
        (r"https://opt-list.ru/catalog/Platy-Arduino"),
        ("414"),
        ('/Arduino/')
    ),
    (
        # Электронные замки
        (r"https://opt-list.ru/catalog/Jelektronnye-zamki"),
        ("415"),
        ('/electro_lock/')
    ),
    (
        # Пепельницы
        (r"https://opt-list.ru/catalog/Pepelnicy"),
        ("416"),
        ('/Ashtrays/')
    ),
    (
        # Умный дом
        (r"https://opt-list.ru/catalog/Umnyj-dom"),
        ("417"),
        ('/smart_house/')
    ),
    (
        # Игрушки для детей, 3D-ручки
        (r"https://opt-list.ru/catalog/Igrushki"),
        ("419"),
        ('/3D_pen/')
    ),




)


def return_data():
    return input_data


