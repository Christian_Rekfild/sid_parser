import os
import sys


def get_start_id():
    """:return start id. именно с него и будет начинаться заполнение поля id в .xls"""
    path_file_id = os.path.abspath("saves\id.txt")
    file_id = open(path_file_id, "r")
    id = int(file_id.readline())
    file_id.close()

    return id


def get_check_id():
    """ :return check_id. Используется ддя проверки корректного завершения программы и предотвращения
    ошибок, которые связаны с этим"""
    path_file_id = os.path.abspath("saves\check_id.txt")
    file_id = open(path_file_id, "r")
    check_id = int(file_id.readline())
    file_id.close()

    return check_id


def check_id_error():
    """ :return True, если id > check_id. А точнее, если программа отработалась нормально"""
    id = get_start_id()
    check_id = get_check_id()

    if id > check_id:
        print("НАЧИНАЕМ ПАРСИНГ")
        return True
    else:
        sys.exit("ОШИБКА.\nСКОРЕЕ ВСЕГО ВО ВРЕМЯ ПРОШЛОГО СЕАНСА ПРОГРАММА БЫЛА ЗАКРЫТА НЕПРАВИЛЬНО.")


def save_id(id):
    path_file_id = os.path.abspath("saves\id.txt")
    file_id = open(path_file_id, "w")
    file_id.write(id)
    file_id.close()


def save_check_id(check_id):
    path_file_id = os.path.abspath("saves\check_id.txt")
    file_id = open(path_file_id, "w")
    file_id.write(check_id)
    file_id.close()
